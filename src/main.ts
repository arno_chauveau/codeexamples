import "angular";
import "angular-translate";
import {ExampleTypescriptController} from "./controllers/ExampleTypescriptController";

var appModule: ng.IModule = angular.module("com.axxes.masterclass.codeexamples", [])
    .controller("ExampleTypescriptController", () => new ExampleTypescriptController);

angular.bootstrap(document, [appModule.name], {
    strictDi: true
});