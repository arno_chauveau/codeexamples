export function ExampleJSController() {

    var vm = this;

    vm.hello = "";
    vm.greeting = "";

    vm.sayHello = sayHello;

    function sayHello() {
        this.greeting = "Hello, " + vm.hello + "!";
    }
}